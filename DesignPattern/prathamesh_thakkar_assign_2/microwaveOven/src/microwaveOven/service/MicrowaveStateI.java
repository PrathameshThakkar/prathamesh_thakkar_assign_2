package microwaveOven.service;

public interface MicrowaveStateI {
	void pressStart();
	void pressCancel();
	void pressDigits();
	void pressSetClock();
}

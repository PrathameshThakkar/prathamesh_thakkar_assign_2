package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class ClockInitialisedStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	public ClockInitialisedStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;
	}

	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		System.out.println("Changed state to Initial State");
		int selectedDigits = SetClockStateImpl.getClocktime();
		System.out.println("State changed from Clock Initialised to Initial State, clock period is set for " + selectedDigits + " time unit");
		res = "State changed from Clock Initialised to Initial State, clock period is set for " + selectedDigits + " time unit";
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getInitialStateImpl());
	}

	
	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);

	}

	
	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);

	}
	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);

	}
}

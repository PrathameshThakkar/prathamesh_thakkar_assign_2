package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class DisplayTimeOfDayStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	
	public DisplayTimeOfDayStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		res = "Active : But no effect";
		((Results) output).storeNewResult(res);
		System.out.println("Active : But no effect");
	}

	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);
	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Active : But no effect");
		res = "Active : But no effect";
		((Results) output).storeNewResult(res);
	}

	/* (non-Javadoc)
	 * @see microwaveOven.service.MicrowaveStateI#pressSetClock()
	 */
	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Changing State to SetClockState");
		res = "State changed from Display Time Of The Day State to Set Clock State";
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getSetClockStateImpl());
	}
}

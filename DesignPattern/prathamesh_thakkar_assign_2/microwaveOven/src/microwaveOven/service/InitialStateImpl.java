package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class InitialStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	private static int timer;
	public InitialStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;		
	}
	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		res = "Ignored";
		((Results) output).storeNewResult(res);
		System.out.println("Ignored");
	}

	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		res = "Ignored";
		((Results) output).storeNewResult(res);
		System.out.println("Ignored");
	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Change state from Initial to precooking State");
		timer = microWaveContext.getDigits();
		res = "State changed from Initial to Pre-Cooking State";
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getPreCookingStateImpl());
	}
	
	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		res = "Ignored";
		((Results) output).storeNewResult(res);
		System.out.println("Ignored");
	}

	public static int getTimer(){
		return timer;
	}

	public static void setTimer(int value){
		timer = value;
	}
}
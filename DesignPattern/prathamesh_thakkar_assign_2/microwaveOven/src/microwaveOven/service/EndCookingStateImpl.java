package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class EndCookingStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	
	public EndCookingStateImpl(MicrowaveContext microwaveContext2, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext2;
		output = storeResult;
	}

	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		System.out.println("Changing State to Cooking State");
		int number = InitialStateImpl.getTimer();
		res = "State changed from End Cooking to Cooking State, timer = " + number;
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getCookingStateImpl());
	}

	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		//microWaveContext.setDigits(0);
		 InitialStateImpl.setTimer(0);
		 int number = InitialStateImpl.getTimer();
		//System.out.println("Changing State to Display Time Of The Day State and clearing cooking time, timer = " + number);
		res = "State changed from End Cooking to Display Time Of The Day State and clearing cooking time, timer = " + number;
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getDisplayTimeOfDayStateImpl());
	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);
	}
	
	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);
	}

}

/**
 * 
 */
package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

/**
 * @author prathamesh
 *
 */
public class CookingStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	public CookingStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;
	}

	
	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		res= "Inactive";
		System.out.println("Inactive");
		((Results) output).storeNewResult(res);
	}

	/* (non-Javadoc)
	 * @see microwaveOven.service.MicrowaveStateI#pressCancel()
	 */
	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		System.out.println("State changed from Cooking to End Cooking State.");
		int number = InitialStateImpl.getTimer();
		res = "State changed from Cooking to End Cooking State, timer =  " + number ;
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getEndCookingStateImpl());
	}

	/* (non-Javadoc)
	 * @see microwaveOven.service.MicrowaveStateI#pressDigits()
	 */
	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Ignore");
		res = "Ignore";
		((Results) output).storeNewResult(res);
	}

	/* (non-Javadoc)
	 * @see microwaveOven.service.MicrowaveStateI#pressSetClock()
	 */
	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);
	}

}

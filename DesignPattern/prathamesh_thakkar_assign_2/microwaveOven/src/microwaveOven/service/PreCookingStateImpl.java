package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class PreCookingStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private FileDisplayInterface output;
	private String res;
	public PreCookingStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;
	}
	
	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		System.out.println("Change state to Cooking");
		int time = InitialStateImpl.getTimer();
		res = "State changed from Pre-cooking to Cooking State."
				+ " Cooking for "+ time + " time unit" ;
		((Results) output).storeNewResult(res);
		microWaveContext.setState(microWaveContext.getCookingStateImpl());
	}

	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		System.out.println("Ignore");
		res = "Ignore";
		((Results) output).storeNewResult(res);
	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignore";
		((Results) output).storeNewResult(res);
	}

	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignore";
		((Results) output).storeNewResult(res);
	}
}

package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;

public class MicrowaveContext implements MicrowaveStateI {
	private	MicrowaveStateI cookingStateImpl;
	private MicrowaveStateI initialStateImpl;
	private MicrowaveStateI clockInitialisedStateImpl;
	private MicrowaveStateI displayTimeOfDayStateImpl;
	private MicrowaveStateI endCookingStateImpl;
	private MicrowaveStateI preCookingStateImpl;
	private MicrowaveStateI setClockStateImpl;
	private int digits;
	private FileDisplayInterface storeResult;
	
	public int getDigits() {
		return digits;
	}

	public void setDigits(int digits) {
		this.digits = digits;
	}
	private MicrowaveStateI currentState;

	public MicrowaveStateI getCurrentState() {
		return currentState;
	}

	public MicrowaveContext(FileDisplayInterface output){
		storeResult = output;
		cookingStateImpl = new CookingStateImpl(this, storeResult);
		initialStateImpl = new InitialStateImpl(this, storeResult);
		clockInitialisedStateImpl = new ClockInitialisedStateImpl(this, storeResult);
		displayTimeOfDayStateImpl = new DisplayTimeOfDayStateImpl(this, storeResult);
		endCookingStateImpl = new EndCookingStateImpl(this, storeResult);
		preCookingStateImpl = new PreCookingStateImpl(this, storeResult);
		setClockStateImpl = new SetClockStateImpl(this, storeResult);
		currentState = initialStateImpl;
		
	}

	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		currentState.pressStart();

	}

	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		currentState.pressCancel();
	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		currentState.pressDigits();
	}

	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		currentState.pressSetClock();
	}

	void setState(MicrowaveStateI state){
		currentState = state;
	}

	public MicrowaveStateI getCookingStateImpl() {
		return cookingStateImpl;
	}

	public MicrowaveStateI getInitialStateImpl() {
		return initialStateImpl;
	}

	public MicrowaveStateI getClockInitialisedStateImpl() {
		return clockInitialisedStateImpl;
	}

	public MicrowaveStateI getDisplayTimeOfDayStateImpl() {
		return displayTimeOfDayStateImpl;
	}

	public MicrowaveStateI getEndCookingStateImpl() {
		return endCookingStateImpl;
	}

	public MicrowaveStateI getPreCookingStateImpl() {
		return preCookingStateImpl;
	}

	public MicrowaveStateI getSetClockStateImpl() {
		return setClockStateImpl;
	}
	public void clear(){
		setState(initialStateImpl);
	}

}

package microwaveOven.service;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.Results;

public class SetClockStateImpl implements MicrowaveStateI {
	MicrowaveContext microWaveContext;
	private String res;
	private FileDisplayInterface output;
	private  static int clocktime;
	public SetClockStateImpl(MicrowaveContext microwaveContext, FileDisplayInterface storeResult) {
		microWaveContext = microwaveContext;
		output = storeResult;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void pressStart() {
		// TODO Auto-generated method stub
		System.out.println("Inactive");
		res = "Inactive";
		((Results) output).storeNewResult(res);

	}
	
	@Override
	public void pressCancel() {
		// TODO Auto-generated method stub
		System.out.println("Inactive");
		res = "Inactive";
		((Results) output).storeNewResult(res);

	}

	@Override
	public void pressDigits() {
		// TODO Auto-generated method stub
		System.out.println("If pressed three or four digits change state to Clock Initialised State");
		int number = microWaveContext.getDigits();
		//int length = String.valueOf(number).length();
		int length = (int)(Math.log10(number)+1);
		System.out.println("Length of number: " + length);
		if(length == 3 || length == 4){
			clocktime = number;
			res = "State changed from Set Clock to Clock Initialised State";
			microWaveContext.setState(microWaveContext.getClockInitialisedStateImpl());
			((Results) output).storeNewResult(res);
			//System.out.println("Timer is set for " + number + "time unit");
		}
		else if(length > 4){
			res = "Beep Beep Beep";
			((Results) output).storeNewResult(res);
			System.out.println("Beep Beep Beep");
		}
		
	}

	@Override
	public void pressSetClock() {
		// TODO Auto-generated method stub
		System.out.println("Ignored");
		res = "Ignored";
		((Results) output).storeNewResult(res);

	}

	public static  int getClocktime() {
		return clocktime;
	}
}

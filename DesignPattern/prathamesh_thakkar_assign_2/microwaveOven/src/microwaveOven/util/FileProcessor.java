package microwaveOven.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileProcessor {
	private Scanner in = null;

	public FileProcessor(String fileName){
		File inputFile = new File(fileName);
		try {
			in = new Scanner(inputFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("File not found.");
			e.printStackTrace();
			System.exit(0);
		}
		finally{
			
		}
	}
	public String readLine(){
		String line = null;
		while(in.hasNextLine()){
			line = in.nextLine();
			return line;
		}
		line = null;
		in.close();
		return line;
	}
}

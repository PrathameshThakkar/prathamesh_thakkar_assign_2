package microwaveOven.driver;

import microwaveOven.service.MicrowaveContext;
import microwaveOven.service.MicrowaveStateI;
import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.FileProcessor;
import microwaveOven.util.Results;

public class Driver {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inputFileName = null;
		String outputFileName = null;
		//print the string and insert that string in braces.
		System.out.println(args.length);
		if(args.length != 2) {
			System.err.println("Expected 2 args");
			System.exit(1);
		}
		else{
			inputFileName = args[0];
			System.out.println("INPUT: " +inputFileName);
			outputFileName = args[1];
			//System.out.println("OP: " + outputFileName.charAt(0));
			System.out.println("OUTPUT: " +outputFileName);
		}
		FileProcessor fp = new FileProcessor(inputFileName);
		FileDisplayInterface output = new Results();
		MicrowaveContext context = new MicrowaveContext(output);
		String line = null;
		while((line = fp.readLine()) != null){
			try{
				int parseInt = Integer.parseInt(line);
				context.setDigits(parseInt);
				System.out.println(parseInt);
				context.pressDigits();
			}catch(NumberFormatException e){
				if (line.equalsIgnoreCase("START")){
					context.pressStart();
				}
				else if(line.equalsIgnoreCase("CANCEL")){
					context.pressCancel();
				}
				else if(line.equalsIgnoreCase("SET CLOCK")){
					context.pressSetClock();
				}
				else if(line.equalsIgnoreCase("CLEAR")){
					context.clear();
				}
				else{
					String res = "Invalid input skipped";
					((Results) output).storeNewResult(res);
				}
			}
		}
		MicrowaveStateI state = context.getCurrentState();
		System.out.println(state);
		output.writeToFile(outputFileName);
	}
}